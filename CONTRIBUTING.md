# Contributing

I currently don't accept contributions in terms of code (pull/merge
requests), but you can [submit issues and fill bug
reports](https://framagit.org/Gee/sosage/-/issues)

## Why don't you accept contributions?

Managing contributions is a real job that I'm not ready to take on: it
involves rereading pull/merge requests, dialoguing with the developers
who submitted them, explaining rejections, keeping track of work in
progress, following up the accepted ones, testing…  basically, that it
involves spending time, energy and attention that I'd rather spend on
other things.

_Sosage_ remains a free software, and you have every right to modify
it as you see fit and to share those modifications: you'll just have
to do that on your own fork.
