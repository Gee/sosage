/*
  [src/Sosage/Utils/profiling.cpp]
  Tools for profiling code.

  ===========================================================================

  This file is part of SOSAGE, released under the MIT License.
  Copyright (c) 2019-present Simon Giraudot

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
  DEALINGS IN THE SOFTWARE.

  ===========================================================================

  Author(s): Simon Giraudot <sosage@ptilouk.net>
*/

#include <Sosage/Utils/error.h>
#include <Sosage/Utils/profiling.h>

#include <algorithm>
#include <cmath>

namespace Sosage
{

Timer::Timer (const std::string& id, bool master)
  : m_id (id), m_master(master)
#ifndef SOSAGE_PROFILE_FINELY
, m_duration(0), m_nb(0)
#endif
{ }

Timer::~Timer()
{
  if (m_master)
  {
#ifdef SOSAGE_PROFILE
    debug << "[Profiling " << m_id << "] ";
#ifdef SOSAGE_PROFILE_FINELY
    debug <<  << std::endl;
#endif
    display();
#endif
  }
}

void Timer::start()
{
  m_start = Time::now();
#ifndef SOSAGE_PROFILE_FINELY
  ++ m_nb;
#endif
}

void Timer::stop()
{
#ifdef SOSAGE_PROFILE_FINELY
  m_duration.push_back(Time::now() - m_start);
#else
  m_duration += Time::now() - m_start;
#endif
}

void Timer::display()
{
#ifdef SOSAGE_PROFILE_FINELY

#ifdef SOSAGE_PROFILE_TO_FILE
  if (m_id == "CPU_idle")
  {
    for (auto& t : m_duration)
      t = 100. * (1. - (t / 16.666666));
  }
  std::ofstream ofile (m_id + ".plot");
  for (const auto& t : m_duration)
    ofile << t << std::endl;
  std::size_t smooth = 20;
  if (m_duration.size() > smooth * 3)
  {
    std::ofstream ofile2 (m_id + "_smooth.plot");

    for (std::size_t i = smooth; i < m_duration.size() - smooth; ++ i)
    {
      double mean = 0.;
      std::size_t nb = 0;
      for (std::size_t j = i-smooth; j < i+smooth; ++ j)
      {
        mean += m_duration[j];
        ++ nb;
      }
      ofile2 << mean/nb << std::endl;
    }
  }

#endif

  double total = 0;
  for (const auto& d : m_duration)
    total += d;
  std::sort(m_duration.begin(), m_duration.end());
  debug << "Min = " << to_string(m_duration.front())
      << ", 10% = " << to_string(m_duration[m_duration.size() / 10])
      << ", median = " << to_string(m_duration[m_duration.size() / 2])
      << ", 90% = " << to_string(m_duration[9 * m_duration.size() / 10])
      << ", max = " << to_string(m_duration.back())
      << ", total = " << to_string(total)
      << ", mean = " << to_string(total / m_duration.size()) << std::endl;
#else
  debug << to_string(mean_duration())
        << ((m_nb > 1)
            ? " per iteration (total = " + to_string(m_duration) + " for " + std::to_string(m_nb) + " iterations)"
           : "") << std::endl;
#endif
}

#ifndef SOSAGE_PROFILE_FINELY
double Timer::mean_duration() const
{
  return m_duration / double(m_nb);
}
#endif

std::string Timer::to_string (double d) const
{
  if (m_id == "CPU_idle")
    return std::to_string(d).substr(0,4) + "%";
  if (d < 900)
    return std::to_string(std::round(d * 100) / 100).substr(0,4) + "ms";
  return std::to_string(std::round((d / 1000.) * 100) / 100).substr(0,4) + "s";
}

Counter::Counter (const std::string& id)
  : m_id (id), m_nb(0)
{ }

Counter::~Counter ()
{
  debug << "[Profiling " << m_id << "] " << m_nb << " iteration(s)" << std::endl;
}

void Counter::increment()
{
  ++ m_nb;
}

} // namespace Sosage
